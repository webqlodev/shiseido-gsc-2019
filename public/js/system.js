$(document).ready(function(){

    $('.modal').on('hidden.bs.modal', function (e) {
        $('body').css('padding-right',0);
    })

    var owl = $(".owl-carousel");
    owl.owlCarousel({
        items:1,
        loop:true,
        autoheight:false,
        dots : false,
        nav:true
    });
    var error = $(".help-block.label.label-danger");
    var target = $("#registration_form");
    if( error.length ){
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 1000);
    }
    $("a.footer-social-media img").hover(
      function() {
        $( this ).attr('src','/images/gsc/footer-logo.png');
      }, function() {
        $( this ).attr('src','/images/gsc/footer-logo-white.png');
      }
    );
    $("a#logo-btn").hover(
      function() {
        $( "a#logo-btn img" ).attr('src','/images/gsc/thank-you-logo.png');
      }, function() {
        $( "a#logo-btn img" ).attr('src','/images/gsc/footer-logo-white.png');
      }
    );
});