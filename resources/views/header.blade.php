 <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/" rel="home page"><img class="nav-logo" src="{{ asset('images/gsc/shiseido-logo.png') }}" height="auto" width="auto"></a>
        </div>
        <div class="navbar-container">
            <ul class="nav navbar-nav">
              <li><a href="/#out_going" rel="home page"><span>About</span></a></li>
              <li><a href="/#product-sec"><span>Products</span></a></li>
              <li><a href="/#registration_form"><span>Free Sample</span></a></li>
            </ul>
        </div>
    </div>
</nav>