@extends('home')
@section('form')
<div id="registration_form">
    <div class="panel panel-default">
        <div class="panel-heading text-center panel-custom-css">
            <h2>
                Stay Beautiful Under The Sun Today!
                <br>
                Redeem your 
                <br><span class="blue-text">Shiseido Suncare Sample Kit</span> now. 
            </h2>
        </div>
        <div class="panel-body panel-color-background">
            <form method="post" action="{{ route('submit') }}">
            <div class="form-group @if ( $errors->has('fullname') ) has-error @endif">
                <label class="label-required" for="fullname">Name(as per your NRIC)</label>
                <input id="fullname" class="form-control" type="text" name="fullname" value="{{ old('fullname') }}" placeholder="eg: John Doe" />
                @if ( $errors->has('fullname') )
                    <span class="help-block label label-danger"> {{ $errors->first('fullname') }} </span>
                @endif
            </div>

            <div class="form-group @if ( $errors->has('phone') ) has-error @endif">
                <label class="label-required" for="phone">Mobile No.</label>
                <input id="phone" class="form-control" type="text" name="phone" value="{{ old('phone') }}" placeholder="eg: 0123456789" />
                @if ( $errors->has('phone') )
                    <span class="help-block label label-danger">{{ $errors->first('phone') }}</span>
                @endif
            </div>

            <div class="form-group @if ( $errors->has('email') ) has-error @endif">
                <label class="label-required" for="email">Email</label>
                <input id="email" class="form-control" type="text" name="email" value="{{ old('email') }}" placeholder="eg: john@email.com" />
                @if ( $errors->has('email') )
                    <span class="help-block label label-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="form-group @if ( $errors->has('location') ) has-error @endif checkbox_form">
                <label class="label-required" for="location">I would like to receive my sample at</label>
                <select id="location" class="form-control" name="location">
                    <option value="">Please choose one</option>
                    <optgroup label="KUALA LUMPUR / SELANGOR">
                        <option>ISETAN KLCC</option>
                        <option>ISETAN THE GARDENS MALL</option>
                        <option>ISETAN BANDAR UTAMA</option>
                        <option>ISETAN LOT 10</option>
                        <option>AEON MID VALLEY MEGAMALL</option>
                        <option>AEON BANDAR UTAMA</option>
                        <option>AEON METRO PRIMA</option>
                        <option>AEON TAMAN MALURI</option>
                        <option>AEON BUKIT TINGGI</option>
                        <option>PARKSON PAVILION</option>
                        <option>PARKSON BANDAR UTAMA</option>
                        <option>PARKSON OUG</option>
                        <option>PARKSON SUBANG</option>
                        <option>PARKSON SUNWAY PYRAMID</option>
                        <option>PARKSON SEREMBAN PARADE</option>
                        <option>PARKSON KLANG PARADE</option>
                        <option>PARKSON NU SENTRAL</option>
                        <option>PARKSON VELOCITY</option>
                        <option>SOGO KL</option>
                        <option>SOGO I-CITY MALL</option>
                        <option>SUNWAY PYRAMID</option>
                        <option>ROBINSONS KLCC</option>
                    </optgroup>
                    <optgroup label="EAST COST PENINSULAR">
                        <option>PARKSON EAST COAST MALL</option>
                        <option>PACIFIC KOTA BHARU MALL</option>
                        <option>PACIFIC MENTAKAB</option>
                    </optgroup>
                    <optgroup label="SOUTH PENINSULAR">
                        <option>PARKSON MELAKA</option>
                        <option>PARKSON SQUARE ONE</option>
                        <option>PARKSON HOLIDAY PLAZA</option>
                        <option>AEON BANDARAYA MELAKA</option>
                        <option>AEON TEBRAU CITY</option>
                        <option>AEON BUKIT INDAH</option>
                        <option>THE STORE KOMPLEKS LIEN HOE</option>
                        <option>THE STORE MUAR</option>
                        <option>PACIFIC MALL BATU PAHAT</option>
                        <option>PARKSON PARADIGM MALL (JOHOR BAHRU)</option>
                    </optgroup>
                    <optgroup label="CENTER / NORTHEN PENINSULAR">
                        <option>AEON KINTA CITY</option>
                        <option>AEON QUEENSBAY MALL</option>
                        <option>PARKSON IPOH</option>
                        <option>PARKSON GURNEY PLAZA</option>
                        <option>PARKSON SUNWAY CARNIVAL</option>
                        <option>PARKSON AMAN CENTRAL ALOR SETAR</option>
                        <option>PACIFIC PRAI</option>
                        <option>PACIFIC TAIPING MALL</option>
                        <option>SUNSHINE SQUARE</option>
                    </optgroup>
                    <optgroup label="SARAWAK / BRUNEI">
                        <option>PARKSON THE SPRING SHOPPING MALL</option>
                        <option>PARKSON SIBU</option>
                        <option>PARKSON MIRI</option>
                        <option>FARLEY BINTULU</option>
                        <option>HUA HO YAYASAN</option>
                    </optgroup>
                    <optgroup label="SABAH">
                        <option>METRO SURIA KOTA KINABALU</option>
                        <option>PARKSON IMAGO MALL</option>
                    </optgroup>
                </select>
                @if ( $errors->has('location') )
                    <span class="help-block label label-danger">{{ $errors->first('location') }}</span>
                @endif
            </div>

            <div class="form-group checkbox_form">
                <label for="receive_update">I would like to receive Shiseido updates via</label>
                <select id="receive_update" class="form-control" name="receive_update">
                    <option value="">Please choose one</option>
                    <option>SMS</option>
                    <option>E-Newsletter</option>
                    <option>SMS & E-Newsletter</option>
                </select>
            </div>

            <div class="form-group disclaimer-text">
                By clicking "Submit", I agree to receive occasional email updates on Shiseido related promotions and activities
            </div>

            <div class="form-group tnc-text @if ( $errors->has('tnc') ) has-error @endif">
                <div class="checkbox">
                    <label class="label-required"><input type="checkbox" name="tnc" /> I have read and agree the <a data-toggle="modal" data-target="#consent">Consent</a>, <a class='agree-text' data-toggle="modal" data-target="#privacy_policy">Privacy Policy</a> and <a data-toggle="modal" data-target="#tnc">Terms and Conditions</a></label>
                </div>
                @if ( $errors->has('tnc') )
                    <span class="help-block label label-danger">{{ $errors->first('tnc') }}</span>
                @endif
            </div>

            {{ csrf_field() }}
            <div class='col-xs-12 submit-section'>
                <button class="btn btn-default btn-block custom-submit-btn btn-lg" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('modal')
<div class="modal fade" id="privacy_policy" role="dialog" tabindex="-1" role="dialog" aria-labelledby="privacy_policy">
    <div class="modal-dialog modal-lg">               
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title" style="text-align: center; font-weight:bold;">Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div>
                        <p>Shiseido Malaysia Sdn. Bhd. (<strong>“Shiseido”</strong> or <strong>“We”</strong>) is committed to protecting
                            your privacy and ensuring that your Personal Data is protected. For the purposes of this Privacy Policy,
                            "Personal Data" means any personally identifiable data, whether true or not, about an individual who can be
                            identified from that data.</p>
                    </div>
                    <div>
                        <div>
                            <h3>1. APPLICATION</h3>
                            <p>This Privacy Policy explains the types of Personal Data we collect and how we use, disclose, transfer,
                                process and protect that information.</p>
                            <p>We collect Personal Data through, but not limited to, the following means:</p>
                            <div>
                                <ul>
                                    <li>(a) When you shop on or browse <a href="https://www.shiseido.com.my" rel="shiseido website">[https://www.shiseido.com.my]</a> (the <strong>“Website”</strong>);
                                    </li>
                                    <li>(b) When you shop in-store at our physical stores;</li>
                                    <li>(c) When you connect with us through social media or attend our marketing events; and</li>
                                    <li>(d) When you agree and consent to be a member of the Shiseido Membership, whether through
                                        physical or electronic means.
                                    </li>
                                </ul>
                            </div>
                            <p>We may update this Privacy Policy from time to time by posting updated versions on the Website, and/or by
                                sending an e-mail to you. Your continued membership, access to and/or use of the Website will be taken
                                to be your agreement to, and acceptance of, all changes made in each updated version.<br>Please check
                                back regularly for updated information on how we handle your Personal Data.</p>
                        </div>
                        <div>
                            <h3>2. CONSENT</h3>
                            <p>We do not collect, use or disclose your Personal Data without your consent (except where permitted and
                                authorised by law). By providing your Personal Data to us, you hereby consent to us collecting, using,
                                disclosing, transferring, and processing your Personal Data for the purposes set out in Section 3 of
                                this Privacy Policy.</p>
                            <p>The types of Personal Data we collect include, but are not limited to, your: (a) first name and family
                                name; (b) home address; (c) date of birth; (d) email address; and, only if appropriate, your (e) user
                                name and password; (f) billing and delivery address; (g) personal identification number; and (h) other
                                information as may be reasonably required for us to provide you with the Services as defined in Section
                                3 below.</p>
                        </div>
                        <div>
                            <h3>3. PURPOSE</h3>
                            <p>We collect, use, disclose, transfer and process your Personal Data for the purpose of providing services.
                                These services include, but are not limited to:</p>
                            <div>
                                <ul>
                                    <li>1. providing you with information on products and campaigns from us, Shiseido Group and our
                                        third party business partners via email, SMS, and post (where we have your express consent);
                                    </li>
                                    <li>2. allowing you to purchase products and services offered for sale via the Website;</li>
                                    <li>3. facilitating your transactions with us;</li>
                                    <li>4. sending you product samples and/or products;</li>
                                    <li>5. keeping you informed of updates, changes, and developments relating to us and our Services;
                                    </li>
                                    <li>6. notifying you about important changes to this Privacy Policy, and to our other policies or
                                        services;
                                    </li>
                                    <li>7. providing you with personalized consultations;</li>
                                    <li>8. responding to queries or feedback from you;</li>
                                    <li>9. maintaining and operating the Website;</li>
                                    <li>10. managing our administrative and business operations;</li>
                                    <li>11. engaging third party business partners and data processors to perform certain aspects of the
                                        Services;
                                    </li>
                                    <li>12. performing customer profiling, market analysis, and research to improve our product and
                                        service offerings to you;
                                    </li>
                                    <li>13. preventing, detecting and investigating crime and analysing and managing commercial risks;
                                        and
                                    </li>
                                    <li>14. other purposes which are reasonably related to the above.</li>
                                </ul>
                            </div>
                            <p>(collectively, the <strong>"Services"</strong>)</p>
                        </div>
                        <div>
                            <h3>4. WITHDRAWAL OF CONSENT, ACCESS & CORRECTION</h3>
                            <p>If you wish to withdraw your consent to receive information on new products and campaigns, or any other
                                Services, you may do so by:</p>
                            <div>
                                <ul>
                                    <li>1. unsubscribing from our Website;</li>
                                    <li>2. clicking the “Unsubscribe" link in the email(s) we send to you;</li>
                                    <li>3. contacting our Data Protection Officer at the email address below; or</li>
                                    <li>4. writing to us at the address below.</li>
                                </ul>
                            </div>
                            <p>Please note that if you choose not to provide us with certain Personal Data, or to withdraw your consent
                                to our use, disclosure, transfer and/or processing of your Personal Data, we may not be able to provide
                                you with some or all of the Services.</p>
                            <p>We will ensure that the Personal Data in our possession is accurate and complete to the best of our
                                knowledge.</p>
                            <p>You have a right to request for access and correction of your Personal Data. If you would like assistance
                                in accessing and/or correcting your Personal Data, please contact our Data Protection Officer at the
                                email address below. We will get back to you within 21 days.</p>
                        </div>
                        <div>
                            <h3>5. CHILDREN</h3>
                            <p>This Website is directed toward and designed for use by persons aged 16 or older. We do not intend to
                                collect Personal Data from children under 16 years of age, except on some sites specifically directed to
                                children.</p>
                            <p>We protect the Personal Data of children along with the necessary parental consent in the same manner as
                                it protects the Personal Data of adults.</p>
                        </div>
                        <div>
                            <h3>6. THIRD PARTY DISCLOSURE & TRANSFER</h3>
                            <p>We do not disclose or transfer your Personal Data to third parties unless we have clearly asked for and
                                obtained your consent to do so (except where permitted and authorised by law).</p>
                            <p>The Personal Data which you provide to us may be stored, processed, transferred between, and accessed
                                from servers located in the United States and other countries which have laws and regulations that may
                                not guarantee the same level of protection of Personal Data as Malaysia. However, we will take
                                reasonable steps to ensure that your Personal Data is handled in accordance with this Privacy Policy,
                                regardless of where your Personal Data is stored or accessed from.</p>
                            <div>
                                <dl>
                                    <dt>6.1 Disclosure to affiliated companies in the Shiseido Group</dt>
                                    <dd>The Shiseido Group comprises a number of affiliated companies and legal entities located both
                                        within and outside Malaysia. We may disclose, where appropriate and to the extent necessary,
                                        your Personal Data to such affiliated companies and legal entities for the purposes of corporate
                                        reporting, market research and analysis, customer relationship management and other related
                                        legal and business purposes. Please note that we provide our affiliated companies and legal
                                        entities with only the Personal Data they need for such business and legal purposes, and we
                                        require that they protect such Personal Data in accordance with the applicable laws and
                                        regulations and this Privacy Policy, and not use it for any other purpose.
                                    </dd>
                                    <dt>6.2 Disclosure to third party business partners</dt>
                                    <dd>We rely on third party business partners to perform a variety of services on our behalf. In so
                                        doing, Shiseido may let service providers, located both within and outside Malaysia, use your
                                        Personal Data for the marketing and promotion of products, services or events that may be of
                                        interest to you, for market research and analysis, for customer relationship management, and for
                                        the fulfilment of your orders for products and services purchased via the Website. Please note
                                        that we provide our third party business partners with only the Personal Data they need to
                                        perform their services and we require that they protect such Personal Data in accordance with
                                        the applicable laws and regulations and this Privacy Policy, and not use it for any other
                                        purpose.
                                    </dd>
                                    <dt>6.3 Disclosure to third party data processors</dt>
                                    <dd>We may use third party service providers, located both within and outside Malaysia, to help us
                                        maintain and operate the Website, or for other reasons related to the operation of the Website
                                        and Shiseido’s business, and they may receive your Personal Data for these purposes. We only
                                        provide them the Personal Data they need to provide these services on our behalf. We require
                                        these companies to protect the Personal Data in accordance with the applicable laws and
                                        regulations and this Privacy Policy, and to not use the information for any other purpose.
                                    </dd>
                                    <dt>6.4 Other disclosure</dt>
                                    <dd>We may use and disclose your Personal Data to perform your instructions and, as relevant, (a)
                                        comply with legislative and regulatory requirements; (b) protect or defend rights or properties
                                        of customers and employees of Shiseido; and/or (c) take emergency measures for the purpose of
                                        securing the safety of customers, Shiseido, or the general public. We may also disclose and
                                        transfer our personal data to other service providers and/or third parties (which may be located
                                        both within and outside Malaysia) in the context of a merger, acquisition or any other corporate
                                        exercise involving Shiseido.
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div>
                            <h3>7. SECURITY & PROTECTION</h3>
                            <p>We maintain strict procedures, standards, and security arrangements to protect Personal Data in our
                                possession or under our control. Upon receipt of your Personal Data, whether through physical or
                                electronic means of collection, we will make the necessary security arrangements to protect such
                                Personal Data as are reasonable and appropriate in the circumstances. Such arrangements may comprise
                                administrative measures, physical measures, technical measures, or a combination of such measures.</p>
                            <p>When disclosing or transferring your Personal Data over the internet, we take all reasonable care to
                                prevent unauthorised access to your Personal Data. However, no data transmission over the internet can
                                be guaranteed as fully secure and you acknowledge that you submit information over the internet at your
                                own risk.</p>
                        </div>
                        <div>
                            <h3>8. RETENTION OF PERSONAL DATA</h3>
                            <p>We will not retain your Personal Data for any period of time longer than is necessary to serve the
                                purposes set out in this Privacy Policy and any valid business or legal purposes. After this period of
                                time, we will destroy or anonymise any documents containing your Personal Data in a safe and secure
                                manner.</p>
                        </div>
                        <div>
                            <h3>9. GOVERNING LAW</h3>
                            <p>This Privacy Policy is governed by Malaysia law.</p>
                        </div>
                        <div>
                            <h3>10. CONTACT US</h3>
                            <p>If you would like to access or correct any Personal Data which you have provided to us, submit a
                                complaint, or have any queries about your Personal Data, please contact our Data Protection Officer by
                                contacting us at <a
                                        href="mailto:shiseidocamellia@shiseido.com.my">shiseidocamellia@shiseido.com.my</a>.
                                Alternatively, you may write to us at:<br>
                                Shiseido Malaysia Sdn.Bhd.<br>
                                Unit 7-03, Level 7, Menara UAC, No.12, Jalan PJU 7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor
                                Darul Ehsan,Malaysia<br>
                                Tel: 60-3-7719-1888
                            </p>
                        </div>
                        <div>
                            <p>Date: 20 July 2018 </p>
                        </div>
                    </div>
                </div>
                <!-- /#main -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="consent" role="dialog" tabindex="-1" role="dialog" aria-labelledby="consent">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center; font-weight:bold;">Consent</h4>
            </div>
            <div class="modal-body">
                <p>
                    You hereby agree and consent to Shiseido Malaysia Sdn. Bhd. ("Shiseido") collecting, using, disclosing, transferring, and processing your personal data, in accordance with Shiseido's Privacy Policy (accessible online at <a href="https://www.shiseido.com.my/privacypolicy" target="_blank">https://www.shiseido.com.my/privacypolicy</a>).
                    <br />
                    By providing your personal data to us, you agree and consent to; 
                </p>
                <ol type="a">
                    <li>be included in our databases for our sales and marketing opportunities, including for establishing an account for checkout purposes if applicable; and</li>
                    <li>Shiseido providing, or continuing to provide, you with the Services described in Section 3 of Shiseido's Privacy Policy.</li>
                </ol>
                <p>
                    You specifically agree and consent to the transfer of your personal data to affiliated companies within the Shiseido Group and to third party service providers hired by Shiseido to process such data, both within and outside of Malaysia, in accordance with Shiseido's Privacy Policy.
                    <br />
                    You would also like to receive information on products and campaigns from Shiseido, Shiseido Group and our third party business partners via email, SMS and post. 
                </p>
                <ul>
                    <li>I consent to receiving such information as stated above.</li>
                    <li>I consent to Shiseido's use of my skin-related information and health-related information in accordance with Shiseido's Privacy Policy.</li>
                </ul>
            </div><!-- modal body -->
        </div>
    </div>
</div> 
<div class="modal fade" id="tnc" role="dialog" tabindex="-1" role="dialog" aria-labelledby="terms_and_conditions">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center; font-weight:bold;">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <ol>
                    <li>This Campaign is organized by Shiseido Malaysia Sdn Bhd.</li>
                    <li>The exclusive Sample Kit are while stocks last only. One redemption per customer please. Duplicate redemptions will not be entertained.</li>
                    <li>The Campaign will run on Facebook from 1<sup>st</sup> May 2019 until 30th June 2019.</li> 
                    <li>The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.</li>
                    <li>The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).</li>
                    <li>The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.</li>
                    <li>The Organizer reserves the right at its absolute discretion to substitute any of the mystery gift at any time without prior notice. All mystery gifts are given on an “as is” basis.</li>
                    <li>Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.</li>
                    <li>This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.</li>
                </ol>
            </div><!-- modal body -->
        </div>
    </div>
</div>
@endpush
@push('js')
<script>
$(function () {
    $('input').change(function () {
        $(this).parent('.has-error').removeClass('has-error').find('.help-block').remove();
    });
});
</script>
@endpush
