@extends('home')
@section('form')
<div id="end" class="text-center">
    <p class="lead">
        The free sample event has ended on {{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
    </p>
    <p>
        Thank you for your participation. Please stay tuned to our upcoming events.
    </p>
</div>
@endsection
