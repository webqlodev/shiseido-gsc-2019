@extends('layouts.email')

@section('content')
<table>
    <tr>
        <td style="text-align: center;">
             <img src="{{ asset('images/gsc/shiseido-logo.png') }}" alt="Logo" style="width:300px; margin-bottom: 50px;" />
        </td>
    </tr>
    <tr>
        <td>
            <p>Dear {{ $user->fullname }},</p>

            <h1>Thank you for your interest!</h1>

            <p>
                You may redeem this exclusive <strong style="color: #6e9cd0;">Shiseido Suncare Sample Kit</strong> by presenting this verification code <strong style="color: #6e9cd0;">{{ $user->unique_code }}</strong> and your identity card at your preferred Shiseido store location at <br />
                <strong style="color: #6e9cd0;">{{ $user->redeem_location }}</strong>
            </p>

            <h4>Following is the info you provided to us:</h4>

            <p>
                <ul>
                    <li><strong>Your Name:</strong> {{ $user->fullname }}</li>
                    <li><strong>Email address:</strong> {{ $user->email }}</li>
                    <li><strong>Phone number:</strong> {{ $user->phone }}</li>
                </ul>
            </p>

            <p>Thank you for your participation, have a lovely day!</p>

            <p>
                <a href="{{ route('redirect', ['target' => 'facebook-email']) }}" target="_blank">Follow Shiseido on Facebook</a>
            </p>

            <p>
                Terms & Conditions : 
                <br/>
                    The exclusive Shiseido Suncare Sample Kit is while stocks last only.
                <br/>
                    One redemption per customer please.
                <br/>
                    Duplicate redemptions will not be entertained
                <br/>
                The campaign will run from 1<sup>st</sup> May 2019 at 12:00:00am until 30th June 2019 11:59:59pm.
            </p>
        </td>
    </tr>
</table>
@endsection
