<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- uncomment during live and open GA goal -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89145400-13"></script> -->
    <script>
      // window.dataLayer = window.dataLayer || [];
      // function gtag(){dataLayer.push(arguments);}
      // gtag('js', new Date());

      // gtag('config', 'UA-89145400-13');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Tab icon -->
    <link rel="icon" href="{{ asset('images/gsc/footer-logo.png') }}">

    <meta property="og:url"           content="{{ env('APP_URL') }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="SHISEIDO GSC" />
    <meta property="og:description"   content="Play more with the sweat-activated protection of wetforce. Maintain a fresh, healthy glow with quick dry technology. Experience 100% beautiful protection. Made with soul." />
    <meta property="og:image"         content="{{ asset('images/gsc/Banner_right.jpg') }}" />

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('plugin/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sunsquad.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    @stack('css')
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
    <!-- Modals -->
    @stack('modal')
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script defer src="{{ asset('plugin/OwlCarousel2-2.3.4/dist/owl.carousel.js') }}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/system.js') }}"></script> 
    @stack('js')
</body>
</html>
