<div id="carousel-video" class="carousel slide multi-item-carousel" data-ride="carousel" data-interval="false">
  <div class="carousel-inner carousel-video" role="listbox">
    <div class="item active">
      <div class="item__third">
        <div class="video-wrap">
            <div class="video-container">
               <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_1nFanR4WwA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="item__third">
        <div class="video-wrap">
            <div class="video-container">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/MkClVVD46JU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="item__third">
        <div class="video-wrap">
            <div class="video-container">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/2k9oe2Q15Ro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
      </div>
    </div>
  </div>
  <a id="video-back" href="#carousel-video" class="left carousel-control" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
  <a id="video-next" href="#carousel-video" class="right carousel-control" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
</div>