@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Registration by Date</strong></div>

            <div class="panel-body">
                <table id="total_registration_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Total Registration by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if ( Auth::user()->username == 'webqlo' )
        <div class="col-md-6">
            <blockquote>
                <strong>End Time: </strong>{{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
            </blockquote>

            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Link Clicks</strong></div>

                <div class="panel-body">
                    <table id="total_registration_table" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Facebook</th>
                                <th>Facebook (from email)</th>
                                <th>YouTube</th>
                                <th>Home page</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'facebook')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'facebook-email')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'youtube')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'homelink')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Registration List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-registration-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="registration_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Full Na</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th>Redeem Code</th>
                            <th>Redeem Location</th>
                            @if ( Auth::user()->username == 'webqlo' )
                            <th>Register Time</th>
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(function () {
    $('#total_registration_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-total-registration') }}',
        columns: [
            {data: 'date', name: 'date'},
            {data: 'total', name: 'total'}
        ]
    });

    $('#registration_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-registration-list') }}',
        order: [],
        columns: [
            {
                data: 'fullname'
            },
            {
                data: 'email'
            },
            {
                data: 'phone'
            },
            {
                data: 'code'
            },
            {
                data: 'location'
            }
            @if ( Auth::user()->username == 'webqlo' )
            ,
            {
                data: 'time'
            },
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/admin/resend-email/' + row.code + '" target="_blank">Resend Email</a>';
                }
            }
            @endif
        ]
    });
});
</script>
@endpush
