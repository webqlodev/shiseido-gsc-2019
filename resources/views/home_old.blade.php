@extends('layouts.app')

@push('css')
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
{{-- <div class="heycha-bg">

</div> --}}
<div class="container-fluid">
    {{-- <div class="free-logo">
        <a href="#registration_form">
            <img class="free-sample-img" src="{{ asset('images/swl/floating-button.png') }}" height="auto" width="auto">
        </a>
    </div> --}}

    <a class="floating-logo-btn" href="#registration_form">
        <img class="animated infinite pulse" src="{{ asset('images/swl/floating-button.png') }}" height="auto" width="auto">
    </a>

    


    <div id="top-section">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#"><img class="nav-logo" src="{{ asset('images/swl/shiseido-logo.png') }}" height="auto" width="auto"></a>
                </div>
                <div class="navbar-container">
                    <ul class="nav navbar-nav">
                      <li><a href="#top-section"><span>Home</span></a></li>
                      <li><a href="#product-sec"><span>Products</span></a></li>
                      <li><a href="#registration_form"><span>Free Sample</span></a></li>
                    </ul>
                </div>
          </div>
        </nav>
        <div class='custom-container container mobile-banner-section'>
                <div class='row hidden-xs'>
                    <div class='col-xs-12 p-0 banner-section'>
                        <img class='img-responsive img-banner' src="{{ asset('images/swl/Banner.jpg') }}">
                    </div>
                </div>
                <div class='row hidden-sm hidden-md hidden-lg '>
                     <div class='col-xs-12 mobile-banner-section'>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/swl/Banner_Mobile1.jpg') }}">
                    </div>
                </div>
                <div class='row hidden-sm hidden-md hidden-lg '>
                    <div class='col-xs-12 mobile-banner-section'>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/swl/Banner_Mobile2.jpg') }}">
                    </div>
                </div>
        </div>
    </div>

    <div id="video-section">
        <div class='custom-container container'>
            <div class="row">
                <div class="normal-text">
                   <span class='video-desc'>Harness the brilliant power of neuroscience.</span>
                   <span class='video-desc'>Experience instant light and clarity  with</span> <span class='video-desc'> sakura-bright complex.</span>
                   <span class='video-desc'>Reawaken skin's fundamental ability to self-repair</span>
                   <span class='video-desc'>Visibly improve dark spots: 93%*.</span> <span class='video-desc'> Made with soul.</span>
                </div>
                <h4 class="red-text lucent-title">New White Lucent Brightening Gel Cream.</h4>
                <div class="video-container">
                    <iframe class='video-iframe' width="560" height="280" src="https://www.youtube.com/embed/WtZ6KPOUv4U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-section" id="product-sec">

        <h2 class=" red-text">Skin Brightening Solutions</h2>

        <div class='custom-container container slider-container'>
            <div class='row'>
                <div class="col-xs-12 col-md-12 col-centered">
                    <div class='owl-carousel'>
                        
                        <div class="item">
                            <div class="carousel-col">
                                <div class="carousel-product">
                                    <img class="carousel-product-img product1" src="{{ asset('images/swl/Product1.png') }}" height="auto" width="auto">
                                    <!-- <img class="carousel-product-img hidden-sm hidden-md hidden-lg" src="{{ asset('images/swl/Product1.png') }}" height="auto" width="auto"> -->
                                </div>
                                <div class="carousel-content">
                                    <div class="carousel-title red-text">
                                       BRIGHTENING DAY EMULSION
                                    </div>
                                    <div class="carousel-text">
                                        A featherweight fluid that targets dark spots and dullness while preventing skin damage from UV rays.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="carousel-col">
                                <div class="carousel-product">
                                    <img class="carousel-product-img" src="{{ asset('images/swl/Product2.png') }}" height="auto" width="auto">
                                    <!-- <img class="carousel-product-img hidden-sm hidden-md hidden-lg" src="{{ asset('images/swl/mobile_product2.png') }}" height="auto" width="auto"> -->
                                </div>
                                <div class="carousel-content active">
                                    <div class="carousel-title red-text">
                                        BRIGHTENING GEL CREAM
                                    </div>
                                    <div class="carousel-text">
                                        A smooth gel cream that targets dark spots, dullness, and future skin concerns.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="carousel-col">
                                <div class="carousel-product">
                                    <img class="carousel-product-img" src="{{ asset('images/swl/Product3.png') }}" height="auto" width="auto">
                                     <!-- <img class="carousel-product-img hidden-sm hidden-md hidden-lg" src="{{ asset('images/swl/mobile_product3.png') }}" height="auto" width="auto"> -->
                                </div>
                                <div class="carousel-content">
                                    <div class="carousel-title red-text">
                                        OVERNIGHT CREAM AND MASK
                                    </div>
                                    <div class="carousel-text">
                                        A fresh-yet-rich cream that targets damage incurred during the day and brightens skin.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="custom-container  container form-section">
            <div id="registration_form">
                <div class="panel panel-default">
                    <div class="panel-heading text-center panel-custom-css">
                        <h2>
                            Experience Luminous Skin Today!
                            <br>
                            Get your <span class="red-text">White Lucent</span> Sample Kit now. 
                        </h2>
                        {{-- <h2>Get your <span class="red-text">FREE</span> samples!</h2> --}}
                    </div>

                    <div class="panel-body panel-color-background">
                        @yield('form')
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    {{-- <div class="page-footer">
        <div class="follow-container">
            Follow Us:
        </div>
        <div class="copy-right-text">Copyright by Shiseido Malaysia Sdn Bhd (685030-U).</div>
        <div style="clear: both;"></div>
    </div> --}}

    <div class="page-footer">
        <div class="footer-links-subsection">
            <ul>
                <li>
                    Follow Us:
                    <a class="footer-social-media" target="_blank" href="https://www.shiseido.com.my/"><img class="footer-logo-img" src="images/swl/footer-logo.png" height="auto" width="auto" /></a>
                    <a class="footer-social-media" target="_blank" href="https://www.facebook.com/shiseido.malaysia/"><i class="fa fa-facebook"></i></a>
                    <a class="footer-social-media" target="_blank" href="https://www.youtube.com/channel/UCYTlZzQINEfwcQFq-yJv7cQ"><i class="fa fa-youtube-play"></i></a>
                </li>
            </ul>
        </div>
        <div class="footer-copyright">Copyright by Shiseido Malaysia Sdn Bhd (685030-U).</div>
        <div style="clear: both;"></div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('js/smooth-scroll.js') }}"></script>
<script>
if($(window).width() <= 767){
    $('#carousel .item:nth-child(1)').removeClass('active');
    $('#carousel .item:nth-child(2)').addClass('active');
}

$('.carousel[data-type="multi"] .item').each(function() {
    var next = $(this).next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this)).addClass('middle');

    for (var i = 0; i < 4; i++) {
        next = next.next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));
    }
});
</script>
@endpush
