@extends('layouts.app')
@push('css')
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
{{-- <div class="heycha-bg">

</div> --}}
<div class="container-fluid">
    <a class="floating-logo-btn" href="#registration_form">
        <img class="animated infinite pulse" src="{{ asset('images/gsc/floating-button.png') }}" alt="floating-button">
    </a>
    <div id="top-section">
        @include('header')
        <div class='container mobile-banner-section p-0'>
            <div class='row'>
                <div class="hidden-xs d-flex">
                    <div id="out_going" class='out_going_desktop col-sm-7 banner-section m-auto'>
                        <img class='img-responsive img-banner' src="{{ asset('images/gsc/Banner_left.png') }}">
                    </div>
                    <div class='col-sm-5 banner-section'>
                        <img class='img-responsive img-banner m-auto' src="{{ asset('images/gsc/Banner_right.jpg') }}">
                    </div>
                </div>
                <div class="hidden-lg hidden-md hidden-sm">
                    <div class='col-sm-12 banner-section'>
                        <img class='img-responsive img-banner m-auto' src="{{ asset('images/gsc/Banner_right.jpg') }}">
                    </div>
                    <div class='out_going_mobile mobile-img col-sm-12 banner-section'>
                        <img class='img-responsive img-banner' src="{{ asset('images/gsc/Banner_left.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <div id="video-section">
            <div class='container'>
                <div class="row">
                    <div class="normal-text text-center">
                       Play more with the sweat-activated protection of wetforce. 
                       <br>Maintain a fresh, healthy glow with quick dry technology. 
                       <br>Experience 100% beautiful protection. Made with soul. 
                    </div>
                    <h4 class="blue-text lucent-title text-uppercase">NEW SPORTS BB COMPACT. <br>SPORTS BB.</h4>
                    <!-- <div class="video-container">  -->
                        {{--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/_1nFanR4WwA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
                        @include('video-carousel')
                    <!-- </div> -->
                </div><!-- row -->
            </div>
        </div>
        <div class='container p-0'>
            <div class='technology-section'>
                <div class='row hidden-lg hidden-md'>
                    <div class='col-sm-12'>
                        <div class="technology-detail">
                            <h2 class="blue-text text-uppercase text-left">quick dry technology</h2>
                            <p class="blue-text text-left">SHISEIDO’s new Quick Dry Technology rapidly diffuses sweat to help maintain a fresh, healthy look. Stickiness is wiped out so that makeup stays on.</p>
                        </div>
                        <div class='technology-image'>
                            <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/QuickDry.png') }}">
                        </div>
                    </div>
                    <div class='col-sm-12'>
                        <div class="technology-detail">
                            <h2 class="blue-text text-uppercase text-left">wetforce technology</h2>
                            <p class="blue-text text-left">Exclusive WetForce Technology binds with minerals found in water or perspiration, adding power to the protective UV veil. The more you sweat, the stronger the protective UV veil.</p>
                        </div>
                        <div class='technology-image'>
                            <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/Wetforce.png') }}">
                        </div>
                    </div>
                </div>
                <div class="row d-flex hidden-sm hidden-xs">
                    <div class='col-md-6 technology-detail'>
                        <h2 class="blue-text text-uppercase text-left">quick dry technology</h2>
                        <p class="blue-text text-left">SHISEIDO’s new Quick Dry Technology rapidly diffuses sweat to help maintain a fresh, healthy look. Stickiness is wiped out so that makeup stays on.</p>
                    </div>
                    <div class='col-md-6 technology-detail'>
                        <h2 class="blue-text text-uppercase text-left">wetforce technology</h2>
                        <p class="blue-text text-left">Exclusive WetForce Technology binds with minerals found in water or perspiration, adding power to the protective UV veil. The more you sweat, the stronger the protective UV veil.</p>
                    </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                     <div class='col-md-6 technology-image'>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/QuickDry.png') }}">
                    </div>
                    <div class='col-md-6 technology-image'>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/Wetforce.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class='container p-0'>
            <div class='proven-result-section'>
                <div class='row'>
                    <div class="col-xs-12">
                        <h2 class="blue-text lucent-title text-uppercase text-left">proven results</h2>
                    </div>
                     <div class='col-md-6 col-sm-12'>
                        <h4 class="blue-text lucent-title text-uppercase text-left">NEW SPORTS BB COMPACT</h4>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/new_sports_bb.png') }}">
                        <div class="small">*Tested by 100 Japanese women in September 2017 when playing sports on a sunny day.</div>
                    </div>
                    <div class='col-md-6 col-sm-12 pt-md-40'>
                        <h4 class="blue-text lucent-title text-uppercase text-left">SPORTS BB</h4>
                        <img class='img-responsive mobile-banner' src="{{ asset('images/gsc/sports_bb.png') }}">
                        <div class="small">*Tested by 100 Japanese women in September 2017 when playing sports on a sunny day.</div>
                    </div>
                </div>
            </div><!-- proven-result-section -->
        </div>
    </div>
    <div id="bottom-section">
        <div class="slider-section ">
            <div id="product-sec"> 
                <div class='container slider-container'>
                    <h2 class="blue-text">BB For Sports And HydroBB Compact For Sports</h2>
                    <div class='row'>
                        <div class="col-xs-12 col-md-12 col-centered">
                            <div class='owl-carousel' id='product-carousel'>   
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img product1" src="{{ asset('images/gsc/Product1.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content">
                                            <div class="carousel-title blue-text">
                                                NEW SPORTS BB COMPACT (RM120)<br>
                                                <span class="black-text">SPF50+</span>
                                            </div>
                                            <div class="carousel-text">
                                                An emulsion compact that instantly refreshes skin – anytime, anywhere.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="carousel-col">
                                        <div class="carousel-product">
                                            <img class="carousel-product-img" src="{{ asset('images/gsc/Product2.png') }}" height="auto" width="auto">
                                        </div>
                                        <div class="carousel-content active">
                                            <div class="carousel-title blue-text">
                                                SPORTS BB (RM170)<br>
                                                <span class="black-text">SPF50+</span>
                                            </div>
                                            <div class="carousel-text">
                                                A quick-dry BB that provides non-sticky, longer lasting protection.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> 
        <div class="container form-section">
            @yield('form')
        </div>
        <div class="container sunsquad-section">
            @include('sunsquad')
        </div>
    </div>
    @include('footer')
</div>
@endsection
@push('js')
<script src="{{ asset('js/smooth-scroll.js') }}"></script>
@endpush
