<div class="sunsquad-inner-section">
	<h2>Meet our <span class="blue-text text-uppercase">#shiseidosunsquad</span> <br>and win awesome prizes!</h2>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">15 June 2019 (Saturday)</div>
			<div class="sunsquad-time">10am - 10pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Sunway Pyramid, LG2 Blue Entrance</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">16 June 2019 (Sunday)</div>
			<div class="sunsquad-time">7am - 1pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Run Out Kuala Lumpur, Cyberjaya</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">17 June 2019 (Monday)</div>
			<div class="sunsquad-time">12pm - 7pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Plaza Mont Kiara, Half Fountain Courtyard</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">18 June 2019 (Tuesday)</div>
			<div class="sunsquad-time">12pm - 7pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Cap Square</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">19 June 2019 (Wednesday)</div>
			<div class="sunsquad-time">12pm - 7pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Plaza Mont Kiara, Half Fountain Courtyard</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">20 June 2019 (Thursday)</div>
			<div class="sunsquad-time">12pm - 6pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>The Weld Shopping Centre, Main Entrance</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">21 June 2019 (Friday)</div>
			<div class="sunsquad-time">3pm - 11pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Plaza Arkadia, The Plaza Fountain</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">22 June 2019 (Saturday)</div>
			<div class="sunsquad-time">12pm - 6pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Infront Bangsar Village Mall  (Jalan Telawi 3)</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">23 June 2019 (Sunday)</div>
			<div class="sunsquad-time">10am - 7pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Desa Park City - The Waterfront, P2 Centre Court</span>
			</div>
			<div class="sunsquad-appearance-title">Special Appearance by:</div>
			<div class="sunsquad-special-appearance d-flex">
				<div class="sunsquad-appearance-img pr-custom">
					<img src="{{ asset('images/new_images/Mark_o_Dea.png') }}" alt="Mark_o_Dea" />
					<p class="text-center">Mark O’dea <br/>From 5pm to 7pm</p>
				</div>
				<div class="sunsquad-appearance-img">
					<img src="{{ asset('images/new_images/Jolene_tiong.png') }}" alt="Jolene_tiong" />
					<p class="text-center">Jolene Tiong <br/>From 5pm to 7pm</p>
				</div>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">24 June 2019 (Monday)</div>
			<div class="sunsquad-time">10am - 8pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>DC Mall, Entrance B</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">25 June 2019 (Tuesday)</div>
			<div class="sunsquad-time">12pm - 6pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>Plaza OSK</span>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">26 June 2019 (Wednesday)</div>
			<div class="sunsquad-time">12pm - 6pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>The Sphere Bangsar, Main Entrance</span>
			</div>
			<div class="sunsquad-appearance-title">Special Appearance by:</div>
			<div class="sunsquad-special-appearance d-flex">
				<div class="sunsquad-appearance-img pr-custom">
					<img src="{{ asset('images/new_images/Sean_Lee.png') }}" alt="Sean_Lee" />
					<p class="text-center">Sean Lee <br/>From 1pm to 3pm</p>
				</div>
				<div class="sunsquad-appearance-img">
					<img src="{{ asset('images/new_images/Alexis_SueAnn.png') }}" alt="Alexis_SueAnn" />
					<p class="text-center">Alexis SueAnn <br/>From 1pm to 3pm</p>
				</div>
			</div>
		</div>
	</div>
	<div class="sunsquad-details-section d-flex">
		<div class="sunsquad-icon"><img src="{{ asset('images/new_images/van_icon.png') }}" alt="van_icon"></div>
		<div class="sunsquad-details">
			<div class="sunsquad-date">27 June 2019 (Thursday)</div>
			<div class="sunsquad-time">12pm - 8pm</div>
			<div class="sunsquad-location">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<span>NU Sentral, LG Outdoor Valet Parking</span>
			</div>
		</div>
	</div>
	<div class="d-flex"><div class="sunsquad-notice"><small>*Do note that the time and location are subject to change without prior notice.</small></div></div>
</div>