<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Excel;
use Datatables;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function DatatablesTotalRegistration()
    {
        $total = new Collection;
        $startDate = new Carbon( env('START_TIME') );
        $endDate = new Carbon( env('END_TIME') );

        // Loop through registrations table by date

        $date = $startDate;

        while ( $date->lte($endDate) && !$date->isTomorrow() ) {
            $total->push([
                'date' => $date->format('j F Y (l)'),
                'total' => DB::table('registrations')->whereBetween( 'created_at', [
                    $date->toDateString(),
                    $date->addDay()->toDateString(),
                ])->count(),
            ]);
        }

        return Datatables::of($total)->make(true);
    }

    public function DatatablesRegistrationList()
    {
        $registrations = DB::table('registrations')->select([
            'id',
            'fullname',
            'email',
            'phone',
            'unique_code as code',
            'redeem_location as location',
            'created_at as time',
        ])->orderBy('created_at', 'desc')->get();

        $registrations->transform(function ($item, $key) {
            $time = new Carbon($item->time);
            $item->time = $time->format('j F Y (l) h:i a');
            return $item;
        });

        return Datatables::of($registrations)->make(true);
    }

    public function resendEmail($code)
    {
        Mail::to( DB::table('registrations')->where('unique_code', $code)->value('email') )->send( new ThankYouForRegistering($code) );

        return 'Email sent. <button onclick="window.close()">Close</button>';
    }

    public function exportRegistrationList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Full Name',
                    'Email Address',
                    'Phone Number',
                    'Redeem Code',
                    'Redeem Location',
                    'Register Time',
                ]);

                $registrations = DB::table('registrations')->orderBy('id', 'asc')->get();

                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);

                    $sheet->row($rowIndex, [
                        $value->fullname,
                        $value->email,
                        $value->phone,
                        $value->unique_code,
                        $value->redeem_location,
                        $time->format('j F Y (l) h:i a'),
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
}
