<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
            return view('form');
        } else {
            return view('end');
        }
    }

    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:registrations,email',
    		'phone' => 'required|regex:/\d+/|max:20|unique:registrations,phone',
            'location' => 'required',
            'tnc' => 'accepted',
        ], [
            'fullname.required' => 'Name is required to participate.',
            'fullname.max' => 'Name is too long.',
            'email.required' => 'Email address is required to participate.',
            'email.email' => 'Email address must be valid.',
            'email.max' => 'Email address is too long.',
            'email.unique' => 'This email address has already been registered.',
            'phone.required' => 'Phone number is required to participate.',
            'phone.regex' => 'Only contains numbers. Plus signs, dashes, and letters are not allowed.',
            'phone.max' => 'Phone number should not exceed 20 characters.',
            'phone.unique' => 'This phone number has already been registered.',
            'location.required' => 'Location must be chosen.',
            'tnc.accepted' => 'Policy must be accepted.',
        ]);

        if ( $validator->fails() ) {
            return back()->withErrors($validator)->withInput();
        }

        // Generate code and create voucher

        $code = $this->generateUniqueCode();

        // Create new registration

        DB::table('registrations')->insert([
            'fullname' => $request->fullname,
            'email' => $request->email,
            'phone' => $request->phone,
            'unique_code' => $code,
            'redeem_location' => $request->location,
            'receive_updates' => $request->receive_update,
            'created_at' => Carbon::now(),
        ]);
        // Send email
        Mail::to( $request->email )->send( new ThankYouForRegistering($code) );

        return redirect()->route('thank-you', ['code' => $code]);
    }

    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();

        if ($registration) {
            return view('thank-you', [
                'code' => $code,
            ]);
        } else {
            abort(404);
        }
    }

    public function redirect($target)
    {
        DB::table('clicks')->insert([
            'target' => $target,
            'created_at' => Carbon::now(),
        ]);

        switch ($target) {
            case 'facebook':
            case 'facebook-email':
                $url = 'https://www.facebook.com/ShiseidoMalaysia/';
                break;
            case 'youtube':
                $url = 'https://youtu.be/_1nFanR4WwA';
                break;
            case 'homelink':
                $url = 'https://www.shiseido.com.my/';
                break;
        }

        return redirect($url);
    }

    public function generateUniqueCode()
    {
        $allowedChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        while (true) {
            $code = '';

            for ($i = 0; $i < 5; $i++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }

         return $code;
    }
}
